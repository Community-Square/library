# Knowledge Collection of C²

## Blog Posts on Community Management

### EN 🇬🇧

- [Ten thoughts on community leadership](https://blog.effenberger.org/2017/11/01/ten-thoughts-on-community-leadership/)

### DE 🇩🇪

- [Zehn Zutaten für eine zufriedene Community](https://blog.effenberger.org/2018/06/30/zehn-zutaten-fuer-eine-zufriedene-community/)